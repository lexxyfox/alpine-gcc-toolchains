* Designed for use with automated CI pipelines.
* Currently for x86_64 only. Looking for help with other architectures.
* I'll sign my stuff when I get around to it :v
* Note to others: .apk files _require_ a **valid** URL field and a **valid** license field, otherwise will cause an "IO ERROR"

```bash
echo https://lexxyfox.gitlab.io/alpine-gcc-toolchains >> /etc/apk/repositories
```

