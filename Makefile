-include .config

GNU_MIRROR = https://ftpmirror.gnu.org/gnu
TARBALL_DIR = .build/tarballs
APK_DIR = public/x86_64
TARGET = x86_64-linux-gnu

.ONESHELL:

.PHONY: tarballs

$(TARBALL_DIR):
	mkdir -p "$@"

$(TARBALL_DIR)/autoconf-2.71.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/autoconf/autoconf-2.71.tar.xz

$(TARBALL_DIR)/automake-1.16.5.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/automake/automake-1.16.5.tar.xz

$(TARBALL_DIR)/binutils-$(CT_BINUTILS_VERSION).tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/binutils/binutils-$(CT_BINUTILS_VERSION).tar.xz

$(TARBALL_DIR)/expat-$(CT_EXPAT_VERSION).tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" https://github.com/libexpat/libexpat/releases/download/R_2_4_1/expat-$(CT_EXPAT_VERSION).tar.xz

$(TARBALL_DIR)/gcc-8.3.0.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/gcc/gcc-8.3.0/gcc-8.3.0.tar.xz

$(TARBALL_DIR)/gdb-7.12.1.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/gdb/gdb-7.12.1.tar.xz

$(TARBALL_DIR)/gettext-0.19.8.1.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/gettext/gettext-0.19.8.1.tar.xz

$(TARBALL_DIR)/glibc-2.28.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/glibc/glibc-2.28.tar.xz

$(TARBALL_DIR)/gmp-6.1.2.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/gmp/gmp-6.1.2.tar.xz

$(TARBALL_DIR)/isl-$(CT_ISL_VERSION).tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" https://libisl.sourceforge.io/isl-$(CT_ISL_VERSION).tar.xz

$(TARBALL_DIR)/libiconv-1.16.tar.gz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/libiconv/libiconv-1.16.tar.gz

$(TARBALL_DIR)/linux-4.20.9.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" https://mirrors.kernel.org/pub/linux/kernel/v4.x/linux-4.20.9.tar.xz

$(TARBALL_DIR)/mpc-1.2.1.tar.gz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/mpc/mpc-1.2.1.tar.gz

$(TARBALL_DIR)/mpfr-4.1.0.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/mpfr/mpfr-4.1.0.tar.xz

$(TARBALL_DIR)/ncurses-6.3.tar.gz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" $(GNU_MIRROR)/ncurses/ncurses-6.3.tar.gz

$(TARBALL_DIR)/zlib-1.2.11.tar.xz: $(TARBALL_DIR)
	test -f "$@" || wget -cO "$@" http://downloads.sourceforge.net/project/libpng/zlib/1.2.11/zlib-1.2.11.tar.xz

tarballs: \
	$(TARBALL_DIR)/autoconf-2.71.tar.xz \
	$(TARBALL_DIR)/automake-1.16.5.tar.xz \
	$(TARBALL_DIR)/binutils-$(CT_BINUTILS_VERSION).tar.xz \
	$(TARBALL_DIR)/expat-$(CT_EXPAT_VERSION).tar.xz \
	$(TARBALL_DIR)/gcc-8.3.0.tar.xz \
	$(TARBALL_DIR)/gdb-7.12.1.tar.xz \
	$(TARBALL_DIR)/gettext-0.19.8.1.tar.xz \
	$(TARBALL_DIR)/glibc-2.28.tar.xz \
	$(TARBALL_DIR)/gmp-6.1.2.tar.xz \
	$(TARBALL_DIR)/isl-$(CT_ISL_VERSION).tar.xz \
	$(TARBALL_DIR)/libiconv-1.16.tar.gz \
	$(TARBALL_DIR)/linux-4.20.9.tar.xz \
	$(TARBALL_DIR)/mpc-1.2.1.tar.gz \
	$(TARBALL_DIR)/mpfr-4.1.0.tar.xz \
	$(TARBALL_DIR)/ncurses-6.3.tar.gz \
	$(TARBALL_DIR)/zlib-1.2.11.tar.xz

merge_config.sh:
	wget https://raw.githubusercontent.com/torvalds/linux/master/scripts/kconfig/merge_config.sh
	chmod a+x merge_config.sh

config: merge_config.sh
	ct-ng x86_64-unknown-linux-gnu
	sed -ie 's ^ CT_ ' config.ini
	./merge_config.sh -m -n .config config.ini 1> /dev/null

build: tarballs
	ct-ng build.$(shell nproc)

dist/binutils-$(TARGET): build/usr/bin build/usr/$(TARGET)
	mkdir -p $@/usr/bin $@/usr/$(TARGET)
	mv -t $@/usr/bin/ \
	  $</$(TARGET)-addr2line \
	  $</$(TARGET)-ar \
	  $</$(TARGET)-as \
	  $</$(TARGET)-c++filt \
	  $</$(TARGET)-dwp \
	  $</$(TARGET)-elfedit \
	  $</$(TARGET)-gprof \
	  $</$(TARGET)-ld \
	  $</$(TARGET)-ld.bfd \
	  $</$(TARGET)-ld.gold \
	  $</$(TARGET)-nm \
	  $</$(TARGET)-objcopy \
	  $</$(TARGET)-objdump \
	  $</$(TARGET)-ranlib \
	  $</$(TARGET)-readelf \
	  $</$(TARGET)-size \
	  $</$(TARGET)-strings \
	  $</$(TARGET)-strip
	mv build/usr/$(TARGET)/bin \
	  $@/usr$(TARGET)/

dist/g++-$(TARGET): build/usr/bin
	mkdir -p $@/usr/bin
	mv -t $@/usr/bin/ \
	  $</$(TARGET)-c++ \
	  $</$(TARGET)-g++

dist/gcc-$(TARGET): build/usr/bin build/usr/libexec
	mkdir -p $@/usr/bin
	mv -t $@/usr/bin/ \
	  $</$(TARGET)-cc \
	  $</$(TARGET)-cpp \
	  $</$(TARGET)-gcc-8.3.0 \
	  $</$(TARGET)-gcc-ar \
	  $</$(TARGET)-gcc-nm \
	  $</$(TARGET)-gcc-ranlib 
	mv build/usr/libexec $@/usr/
	$(RM) $</$(TARGET)-gcc
	ln -s $(TARGET)-gcc-8.3.0 \
	  $@/usr/bin/$(TARGET)-gcc

dist/gcov-$(TARGET): build/usr/bin
	mkdir -p $@/usr/bin
	mv -t $@/usr/bin/ \
	  $</$(TARGET)-gcov \
	  $</$(TARGET)-gcov-dump \
	  $</$(TARGET)-gcov-tool

dist/gdb-$(TARGET): build/usr/bin
	mkdir -p $@/usr/bin
	mv $</$(TARGET)-gdb $@/usr/bin/

dist/libc-bin-$(TARGET): build/usr/bin
	mkdir -p $@/usr/bin
	mv $</$(TARGET)-ldd $@/usr/bin/

dist/buildroot-$(TARGET)/usr/$(TARGET): build/usr/$(TARGET)
	mkdir -p "$@"
	mv $</debug-root $</include $</lib $</lib64 $</sysroot $@/

dist/buildroot-$(TARGET): dist/buildroot-$(TARGET)/usr/$(TARGET)

dist/libgcc-dev-$(TARGET)/usr/lib/gcc: build/usr/lib/gcc
	mkdir -p "$@"
	mv $</$(TARGET) $@/

dist/libgcc-dev-$(TARGET): dist/libgcc-dev-$(TARGET)/usr/lib/gcc
	
$(APK_DIR):
	mkdir -p "$@"

$(APK_DIR)/binutils-$(TARGET)-$(CT_BINUTILS_VERSION)-r0.apk: dist/binutils-$(TARGET) binutils.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack "$(CT_BINUTILS_VERSION)-r0" usr < ../../binutils.ini > "../../$@"

$(APK_DIR)/g++-$(TARGET)-8.3.0-r0.apk: dist/g++-$(TARGET) g++.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '8.3.0-r0' usr < ../../g++.ini > "../../$@"

$(APK_DIR)/gcc-$(TARGET)-8.3.0-r0.apk: dist/gcc-$(TARGET) gcc.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '8.3.0-r0' usr < ../../gcc.ini > "../../$@"

$(APK_DIR)/gcov-$(TARGET)-8.3.0-r0.apk: dist/gcov-$(TARGET) gcov.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '8.3.0-r0' usr < ../../gcov.ini > "../../$@"

$(APK_DIR)/gdb-$(TARGET)-7.12.1-r0.apk: dist/gdb-$(TARGET) gdb.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '7.12.1-r0' usr < ../../gdb.ini > "../../$@"

$(APK_DIR)/libc-bin-$(TARGET)-1.24.0-r0.apk: dist/libc-bin-$(TARGET) libc-bin.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '1.24.0-r0' usr < ../../libc-bin.ini > "../../$@"

$(APK_DIR)/buildroot-$(TARGET)-4.20.9-r0.apk: dist/buildroot-$(TARGET) buildroot.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '4.20.9-r0' usr < ../../buildroot.ini > "../../$@"

$(APK_DIR)/libgcc-dev-$(TARGET)-8.3.0-r0.apk: dist/libgcc-dev-$(TARGET) libgcc-dev.ini $(APK_DIR) apk-pack
	cd $<
	../../apk-pack '8.3.0-r0' usr < ../../libgcc-dev.ini > "../../$@"

$(APK_DIR)/crosstool-ng-1.24.0-r0.apk: $(APK_DIR)
	cd $<
	wget https://lexxyfox.gitlab.io/alpine-crosstool-ng/x86_64/crosstool-ng-1.24.0-r0.apk

$(APK_DIR)/APKINDEX.tar.gz: $(APK_DIR) \
	$(APK_DIR)/binutils-$(TARGET)-$(CT_BINUTILS_VERSION)-r0.apk \
	$(APK_DIR)/g++-$(TARGET)-8.3.0-r0.apk \
	$(APK_DIR)/gcc-$(TARGET)-8.3.0-r0.apk \
	$(APK_DIR)/gcov-$(TARGET)-8.3.0-r0.apk \
	$(APK_DIR)/gdb-$(TARGET)-7.12.1-r0.apk \
	$(APK_DIR)/libc-bin-$(TARGET)-1.24.0-r0.apk \
	$(APK_DIR)/buildroot-$(TARGET)-4.20.9-r0.apk \
	$(APK_DIR)/crosstool-ng-1.24.0-r0.apk \
	$(APK_DIR)/libgcc-dev-$(TARGET)-8.3.0-r0.apk
	apk index --no-warnings -Uvo "$@" $(APK_DIR)/*.apk


